import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';


import '../helpers/route_helper.dart';

class MyApp extends StatefulWidget {
  final Store store;

  MyApp(this.store);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    NavigatorObserver navigatorObserver = NavigatorObserver();
    return StoreProvider<AppState>(
      store: widget.store,
      child: MaterialApp(
        title: 'Flutter Demo',
        navigatorObservers: [
          navigatorObserver,
        ],
        onGenerateRoute: (RouteSettings settings) => RouteHelper.instance.onGenerateRoute(settings),
      ),
    );
  }
}
