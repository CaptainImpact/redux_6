import 'package:flutter/material.dart';
import 'package:redux_practice_6/ui/pages/first_counter_page/first_page.dart';
import 'package:redux_practice_6/ui/pages/second_counter_page/second_counter_page.dart';
import 'package:redux_practice_6/ui/pages/third_counter_page/third_counter_page.dart';

import '../res/const.dart';


class RouteHelper {
  // region [Initialize]
  static const String TAG = '[RouteHelper]';

  RouteHelper._privateConstructor();

  static final RouteHelper _instance = RouteHelper._privateConstructor();

  static RouteHelper get instance => _instance;

  // endregion
  Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case FIRST_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: FirstCounterPage(),
        );

      case SECOND_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: SecondCounterPage(),
        );
      case THIRD_PAGE_ROUTE:
        return _defaultRoute(
          settings: settings,
          page: ThirdCounterPage(),
        );

      default:
        return _defaultRoute(
          settings: settings,
          page: FirstCounterPage(),
        );
    }
  }

  static PageRoute _defaultRoute(
      {@required RouteSettings settings, @required Widget page}) {
    return MaterialPageRoute(
      settings: settings,
      builder: (BuildContext context) => page,
    );
  }
}
