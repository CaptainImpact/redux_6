import 'package:flutter/material.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';


import 'package:redux/redux.dart';
import 'package:redux_practice_6/application/application.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';

void main() {
  final store = new Store<AppState>(
    AppState.getAppReducer,
    initialState: AppState.initial(),
    middleware: [
      NavigationMiddleware<AppState>(),
    ],
  );

  runApp(MyApp(store));
}
