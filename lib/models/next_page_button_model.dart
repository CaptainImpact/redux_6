import 'package:flutter/material.dart';
import 'package:redux_practice_6/res/const.dart';




class NextPageButton {
  final String route;
  final String title;
  final Color color;

  NextPageButton({this.route, this.title, this.color});

  static List<NextPageButton> _buttons = [
    NextPageButton(
      route: FIRST_PAGE_ROUTE,
      title: 'First page',
      color: Colors.white,
    ),
    NextPageButton(
      route: SECOND_PAGE_ROUTE,
      title: 'Second page',
      color: Colors.green,
    ),
    NextPageButton(
      route: THIRD_PAGE_ROUTE,
      title: 'Third page',
      color: Colors.yellow,
    ),
  ];

  List<NextPageButton> get buttons{
    return[..._buttons];
  }
}
