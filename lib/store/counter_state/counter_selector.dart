
import 'package:redux/redux.dart';

import 'package:redux_practice_6/store/appstate/app_state.dart';

import 'counter_actions.dart';

class CounterSelector {

 static void Function() getIncrementFunction(Store<AppState> store) {
   return () => store.dispatch(IncrementAction());
 }

 static void Function() getDecrementFunction(Store<AppState> store) {
   return () => store.dispatch(DecrementAction());
 }

 static void Function() getClearFunction(Store<AppState> store) {
   return () => store.dispatch(ClearCounterAction());
 }


 static int getCurrentCounter(Store<AppState> store) {
   return  store.state.counterState.counter;
 }

}