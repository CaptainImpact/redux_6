import 'package:flutter/material.dart';
import 'package:redux_practice_6/models/model.dart';



class AddItemAction {
  static int _id = 0;
  final String item;

  AddItemAction(this.item) {
    _id ++ ;
  }

  int get id => _id;
}

class RemoveItemAction {
  final Item item;

  RemoveItemAction(this.item);
}

class RemoveItemsAction {}