import 'dart:collection';

import 'package:flutter/material.dart';


import 'package:redux_practice_6/store/counter_state/counter_state.dart';


class AppState {
  final CounterState counterState;


  AppState({
    @required this.counterState,

  });

  factory AppState.initial() => AppState(
        counterState: CounterState.initial(),

      );

  static AppState getAppReducer(AppState state, dynamic action) {
    const String TAG = '[appReducer]';
    print('$TAG => <appReducer> => action: ${action.runtimeType}');
    return AppState(
      counterState: state.counterState.reducer(action),

    );
  }

}

class Reducer<T> {
  final String TAG = '[Reducer<$T>]';
  HashMap<dynamic, T Function(dynamic)> actions;

  Reducer({
    @required this.actions,
  }) {
    actions.forEach((key, value) {
      if (value == null) throw ('All Functions must be initialize');
    });
  }

  T updateState(dynamic action, T state) {
    if (actions.containsKey(action.runtimeType)) {
      return actions[action.runtimeType](action);
    }
    return state;
  }
}


