
import 'package:flutter/material.dart';

import '../../widgets/drawer.dart';

class MainLayout extends StatelessWidget {
  final Widget appBar;
  final Widget bottomNavigationBar;
  final Widget child;
  final Color bgColor;
  final Widget floatingActionButton;

  MainLayout({
    @required this.child,
    this.appBar,
    this.bottomNavigationBar,
    this.bgColor,
    this.floatingActionButton,
  });

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
          appBar: appBar,
          bottomNavigationBar: bottomNavigationBar,
          backgroundColor: bgColor,
          floatingActionButton: floatingActionButton,
          drawer: CustomDrawer(),
          body: Container(
            width: double.infinity,
            height: double.infinity,
            child: child,
          ),
    );
  }
}