import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';

import 'package:redux/redux.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';

class NavigationSelector {
  static void Function(String route, NavigationType type) navigateToNextPage(Store<AppState> store) {
    return (String route, NavigationType type) {
      store.dispatch(NavigateToAction.push(route));
    };
  }
}
