import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';
import 'package:redux_practice_6/ui/layouts/main_layout.dart';


import '../counter_page_viewmidel.dart';
import 'package:redux_practice_6/widgets/counter.dart';

class FirstCounterPage extends StatelessWidget {
  final int counter = 0;

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterViewModel>(
        converter: CounterViewModel.fromStore,
        builder: (BuildContext context, CounterViewModel vm) {
          ScreenUtil.init(
            context,
            width: 375,
            height: 812,
            allowFontScaling: true,
          );
          return MainLayout(
            appBar: AppBar(
            ),
            child: Container(
              color: Colors.blueGrey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Counter(
                    counterValue: vm.counter.toString(),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
