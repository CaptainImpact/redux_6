import 'package:flutter/foundation.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:redux/redux.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';

import 'navigation_selector.dart';


class NavigationViewModel {
  final void Function(String route, NavigationType type) changePage;

  NavigationViewModel({@required this.changePage});

  static NavigationViewModel fromStore(Store<AppState> store) {
    return NavigationViewModel(
        changePage: NavigationSelector.navigateToNextPage(store));
  }

}
