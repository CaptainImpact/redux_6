import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';

import 'package:redux_practice_6/store/appstate/app_state.dart';
import 'package:redux_practice_6/ui/layouts/main_layout.dart';

import '../counter_page_viewmidel.dart';
import 'package:redux_practice_6/widgets/counter.dart';

class ThirdCounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MainLayout(
      child: StoreConnector<AppState, CounterViewModel>(
          converter: CounterViewModel.fromStore,
          builder: (BuildContext context, CounterViewModel vm) {
            return Container(
              color: Colors.yellow,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Counter(
                    counterValue: (vm.counter * 10).toString(),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
