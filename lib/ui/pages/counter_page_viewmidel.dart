import 'package:flutter/material.dart';
import 'package:redux/redux.dart';
import 'package:flutter/foundation.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';
import 'package:redux_practice_6/store/counter_state/counter_selector.dart';



class CounterViewModel {
  final int counter;


  final void Function() increment;
  final void Function() decrement;

  final void Function() clear;

  CounterViewModel({
    @required this.counter,


    @required this.increment,
    @required this.decrement,

    @required this.clear,

  });

  static CounterViewModel fromStore(Store<AppState> store) {
    return CounterViewModel(
      counter: CounterSelector.getCurrentCounter(store),
      increment: CounterSelector.getIncrementFunction(store),
      decrement: CounterSelector.getDecrementFunction(store),
      clear: CounterSelector.getClearFunction(store),
    );
  }
}
