import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:redux_practice_6/store/appstate/app_state.dart';

import '../ui/pages/counter_page_viewmidel.dart';

class Counter extends StatelessWidget {
  final String counterValue;

  Counter({
    @required this.counterValue,
  });

  @override
  Widget build(BuildContext context) {
    return StoreConnector<AppState, CounterViewModel>(
        converter: CounterViewModel.fromStore,
        builder: (BuildContext context, CounterViewModel vm) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.all(50),
                child: IconButton(
                  icon: Icon(Icons.remove),
                  tooltip: 'Decrease counter by 1',
                  color: Colors.red,
                  iconSize: 36.0.w,
                  onPressed: vm.decrement,
                ),
              ),
              Text(
                counterValue,
                style: TextStyle(
                  fontSize: 24.0.w,
                  fontWeight: FontWeight.bold,
                  height: 1.0,
                ),
              ),
              Container(
                alignment: Alignment.centerRight,
                margin: EdgeInsets.all(50),
                child: IconButton(
                  icon: Icon(Icons.add),
                  tooltip: 'Increase counter by 1',
                  color: Colors.blue,
                  iconSize: 36.0.w,
                  onPressed: vm.increment,
                ),
              ),
            ],
          );
        });
  }
}
