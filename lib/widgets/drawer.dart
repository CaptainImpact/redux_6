import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:flutter_redux_navigation/flutter_redux_navigation.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:redux_practice_6/models/next_page_button_model.dart';
import 'package:redux_practice_6/store/appstate/app_state.dart';
import 'package:redux_practice_6/ui/pages/counter_page_viewmidel.dart';


import '../ui/pages/navigation_view_mode.dart';

class CustomDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenUtil.init(
      context,
      width: 375,
      height: 812,
      allowFontScaling: true,
    );
    return StoreConnector<AppState, NavigationViewModel>(
        converter: NavigationViewModel.fromStore,
        builder: (BuildContext context, NavigationViewModel vm) {
          return StoreConnector<AppState, CounterViewModel>(
              converter: CounterViewModel.fromStore,
              builder: (BuildContext context, CounterViewModel counterViewModel) {
                return Drawer(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.all(40),
                            child: IconButton(
                              icon: Icon(Icons.remove),
                              color: Colors.red,
                              iconSize: 36.0.w,
                              onPressed: counterViewModel.decrement,
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerRight,
                            margin: EdgeInsets.all(50),
                            child: IconButton(
                              icon: Icon(Icons.add),
                              color: Colors.blue,
                              iconSize: 36.0.w,
                              onPressed: counterViewModel.increment,
                            ),
                          ),
                        ],
                      ),
                      Expanded(
                        child: Container(
                          height: 1,
                          child: ListView.builder(
                              itemCount: NextPageButton().buttons.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  height: 200.0.h,
                                  padding: EdgeInsets.all(15.0.w),
                                  child: RaisedButton(
                                    color: NextPageButton().buttons[index].color,
                                    onPressed: () {
                                      vm.changePage(NextPageButton().buttons[index].route, NavigationType.shouldReplace);
                                    },
                                    child: Text(
                                      _buttonTitle(index),
                                    ),
                                  ),
                                );
                              }),
                        ),
                      ),
                    ],
                  ),
                );
              });
        });
  }

  String _buttonTitle(int index) {
    switch (index) {
      case 0:
        return 'First page';
      case 1:
        return 'Second page';
      case 2:
        return 'Third page';
    }
  }
}
